<!DOCTYPE html>
<html>
	<head>
		<?php
		include_once("includes/head.php");
		?>
	</head>
	<body background="images/sory.png" style="background-repeat: no-repeat; background-attachment: fixed;">
		<div id="wrap" style="width: 80%; margin: 0 auto; display: block;" class="container">
			<div class="row">
				<?php
				include_once("includes/main_menu.php");
				?>

            <table width="100%">
            	<tr>
            		<td style="padding: 20px;">
            			<h4>Miembros del Equipo</h1><br>

						<div class="panel table-responsive" style="border-color:rgba(188, 232, 241, 0.9);"> 
							<div class="panel-heading" style="background-color: rgba(51, 122, 183, 0.9); border-color:rgba(188, 232, 241, 0.9);" >
								<h3 class="panel-title" style="color: rgba(255, 255, 255, 0.9);  text-shadow: 0 0 4px #000;">Administración</h3> 
							</div> 
							<div class="panel-body">							
								<table width="100%" class="table table-bordered table-condensed table-hover">
									<tbody>
									  <tr>
									    <td style="vertical-align: middle;"><center><strong><br>dp07Daniel</strong></center><br></td>
									    <td style="vertical-align: middle;"><center><a href="mailto:07daniel@natasha-bot.com">dp07daniel@natasha-bot.com</a></center></td>
									    <td style="vertical-align: middle; color: rgb(26, 0, 255); text-shadow: 0 0 4px #000;"><center><br>Network Admin</center><br></td>
									  </tr>
									  <tr>
									    <td style="vertical-align: middle;"><center><strong><br>Raihaku</strong></center><br></td>
									    <td style="vertical-align: middle;"><center><a href="mailto:raihaku@natasha-bot.com">raihaku@natasha-bot.com</a></center></td>
									    <td style="vertical-align: middle; color: rgb(26, 0, 255); text-shadow: 0 0 4px #000;"><center><br>Network Admin </center><br></td>
									  </tr>
									</tbody>
							    </table>
							</div> 
						</div>
						<?php
						/*
						<div class="panel table-responsive" style="border-color:rgba(188, 232, 241, 0.9);"> 
							<div class="panel-heading" style="background-color: rgba(92, 182, 230, 0.9); border-color:rgba(188, 232, 241, 0.9);" >
								<h3 class="panel-title" style="color: rgba(255, 255, 255, 0.9); text-shadow: 0 0 4px #000;">Operators</h3> 
							</div> 
							<div class="panel-body"> 														
								<table width="100%" class="table table-bordered table-condensed table-hover">
									<tbody>
									  <tr>
									    <td style="vertical-align: middle;"><center><strong><br>DarkShadow</strong></center><br></td>
									    <td style="vertical-align: middle;"><center><a href="mailto:darkshadow@natasha-bot.com">darkshadow@natasha-bot.com</a></center></td>
									    <td style="vertical-align: middle; color: rgb(102, 51, 204); text-shadow: 0 0 4px #000"><center><br>Team Manager</center><br></td>
									  </tr>
									  <tr>
									    <td style="vertical-align: middle;"><center><strong><br>Gandalf</strong></center><br></td>
									    <td style="vertical-align: middle;"><center><a href="mailto:gandalf@natasha-bot.com">gandalf@natasha-bot.com</a></center></td>
									    <td style="vertical-align: middle; color: rgb(6, 147, 16); text-shadow: 0 0 4px #000;"><center><br>Leader</center><br></td>
									  </tr>
									</tbody>
								</table>
							</div> 
						</div>
						<div class="panel table-responsive" style="border-color:rgba(188, 232, 241, 0.9);"> 
							<div class="panel-heading" style="background-color: rgba(217, 237, 247, 0.9); border-color:rgba(188, 232, 241, 0.9);" >
								<h3 class="panel-title" style="color: rgba(49, 112, 143, 0.9);">Staff</h3> 
							</div> 
							<div class="panel-body"> 							 							
								<table width="100%" class="table table-bordered table-condensed table-hover">
									<tbody>
									  <tr>
									    <td style="vertical-align: middle;"><center><strong><br>Blacky</strong></center><br></td>
									    <td style="vertical-align: middle;"><center><a href="mailto:blacky@natasha-bot.com">blacky@natasha-bot.com</a></center></td>
									    <td style="vertical-align: middle; color: rgb(138, 8, 8);"><center><br>Supervisor</center><br></td>
									  </tr>
									  <tr>
									    <td style="vertical-align: middle;"><center><strong><br>Riicesp</strong></center><br></td>
									    <td style="vertical-align: middle;"><center><a href="mailto:riicesp@natasha-bot.com">riicesp@natasha-bot.com</a></center></td>
									    <td style="vertical-align: middle; color: rgb(255, 0, 0);"><center><br>Support</center><br></td>
									  </tr>
									  <tr>
									    <td style="vertical-align: middle;"><center><strong><br>Kurama</strong></center><br></td>
									    <td style="vertical-align: middle;"><center><a href="mailto:kurama@natasha-bot.com">kurama@natasha-bot.com</a></center></td>
									    <td style="vertical-align: middle; color: rgb(63, 63, 63);"><center><br>Trial</center><br></td>
									  </tr>									  
									</tbody>
								</table>
							</div> 
						</div>
					*/	
                    ?>
					</td>					
            		<td width="200px" valign="top" style="padding-top: 25px;">
                        
                        <?php
                        include_once("includes/sidebar.php");
                        ?>
            		</td>
            	</tr>         
			</table>

			</div>

		</div>

		<?php
        include_once("includes/footer.php");
        ?>
	</body>
</html>